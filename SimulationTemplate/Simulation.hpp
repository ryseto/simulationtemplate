//
//  Simulation.hpp
//  SimulationTemplate
//
//  Created by Ryohei Seto on 3/17/16.
//

#ifndef Simulation_hpp
#define Simulation_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <sstream>
#include <ctime>
#include <map>
#include <algorithm>
#include "Global.h"
#include "System.hpp"
#include "Events.h"

class Simulation
{
private:
	System sys;
	std::string indent;
public:
	Simulation();
	~Simulation();
	void simulationSet1(std::string in_args,
						std::vector<std::string>& input_files);
	void setupSimulation(std::string in_args,
						 std::vector<std::string>& input_files);
	void setDefaultParameters();

	void readParameterFile(const std::string& filename_parameters);
	void autoSetParameters(const std::string &keyword, const std::string &value);
	
	ParameterSet p;
	/*********** Events  ************/
	std::list <Event> events;
};


#endif /* Simulation_hpp */
