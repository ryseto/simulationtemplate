//
//  System.cpp
//  SimulationTemplate
//
//  Created by Ryohei Seto on 3/17/16.
//

#include "System.hpp"

System::System(ParameterSet& ps, std::list <Event>& ev):
events(ev),
p(ps),
twodimension(false)
{
	
}

// [0,l]
int System::periodize(vec3d& pos)
{
	if (pos.x >= lx) {
		pos.x -= lx;
	} else if (pos.x < 0) {
		pos.x += lx;
	}
	if (pos.y >= ly) {
		pos.y -= ly;
	} else if (pos.y < 0) {
		pos.y += ly;
	}
	if (pos.z >= lz) {
		pos.z -= lz;
	} else if (pos.z < 0) {
		pos.z += lz;
	}
	return 0;
}
