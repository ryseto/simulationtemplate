//
//  Event.h
//  SimulationTemplate
//
//  Created by Ryohei Seto and Romain Mari on 3/17/16.
//

#ifndef Events_h
#define Events_h

#include <string>

struct Event {
	std::string type;
};
#endif
