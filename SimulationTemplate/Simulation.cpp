//
//  Simulation.cpp
//  SimulationTemplate
//
//  Created by Ryohei Seto on 3/17/16.
//

#include "Simulation.hpp"
using namespace std;

Simulation::Simulation(): sys(System(p, events))
{
	indent = "  Simulation::\t";
};

Simulation::~Simulation()
{
	
};

void Simulation::simulationSet1(std::string in_args,
								std::vector<std::string>& input_files)
{
	setupSimulation(in_args, input_files);

}

void Simulation::setupSimulation(string in_args,
								 vector<string>& input_files)
{
	cout << indent << "Simulation setup starting... " << endl;
	string filename_parameters = input_files[0];
	setDefaultParameters();
	readParameterFile(filename_parameters);
	cout << indent << "Simulation setup [ok]" << endl;
}

void Simulation::setDefaultParameters()
{
	/**
	 \brief Set default values for ParameterSet parameters.
	 */

}



void Simulation::autoSetParameters(const string &keyword, const string &value)
{
	/**
	 \brief Parse an input parameter
	 */
	string numeral, suffix;
	if (keyword == "foo") {
		// p.foo = atoi(value.c_str());
	} else if (keyword == "bar") {
		//p.bar = str2bool(value);
	} else {
		ostringstream error_str;
		error_str  << "keyword " << keyword << " is not associated with an parameter" << endl;
		throw runtime_error(error_str.str());
	}
}

void Simulation::readParameterFile(const string& filename_parameters)
{
	/**
	 \brief Read and parse the parameter file
	 */
	ifstream fin;
	fin.open(filename_parameters.c_str());
	if (!fin) {
		ostringstream error_str;
		error_str  << " Parameter file '" << filename_parameters << "' not found." <<endl;
		throw runtime_error(error_str.str());
	}
	string keyword, value;
	while (!fin.eof()) {
		string line;
		if (!getline(fin, line, ';')) {
			break;
		}
		if (fin.eof()) {
			break;
		}
		string str_parameter;
		removeBlank(line);
		str_parameter = line;
		string::size_type begin_comment;
		string::size_type end_comment;
		do {
			begin_comment = str_parameter.find("/*");
			end_comment = str_parameter.find("*/");
			if (begin_comment > 10000) {
				break;
			}
			str_parameter = str_parameter.substr(end_comment+2);
		} while (true);
		if (begin_comment > end_comment) {
			cerr << str_parameter.find("/*") << endl;
			cerr << str_parameter.find("*/") << endl;
			throw runtime_error("syntax error in the parameter file.");
		}
		string::size_type pos_slashslash = str_parameter.find("//");
		if (pos_slashslash != string::npos) {
			throw runtime_error(" // is not the syntax to comment out. Use /* comment */");
		}
		Str2KeyValue(str_parameter, keyword, value);
		autoSetParameters(keyword, value);
	}
	fin.close();
	return;
}
