//
//  System.hpp
//  SimulationTemplate
//
//  Created by Ryohei Seto on 3/17/16.
//

#ifndef System_hpp
#define System_hpp

#include <list>
#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>

#include "ParameterSet.h"
#include "vec3d.h"
#include "Events.h"

class System{
private:
	std::list <Event>& events;
public:
	System(ParameterSet& ps, std::list <Event>& ev);
	ParameterSet& p;
	int np; ///< number of particles
	double lx; //< box size
	double ly; //< box size
	double lz; //< box size
	bool twodimension;
	std::vector<vec3d> position;
	int periodize(vec3d&);
};

#endif /* System_hpp */
